
var someBool = true;
var someOtherBoolean = false;

if(someBool || !someOtherBoolean){
	console.log("Der Ausdruck war true");
}
else{
	console.log("Der Ausdruck war false");
}

var someString = "Hallo";

var other = "";
switch(someString){
	case "Hallo":
		other = "Der String war 'hallo'";
		break;
	case "Welt":
		other = "Der String war 'welt'";
		break;
	default:
		other = "Der String war weder 'hallo' noch 'welt'";
}

var zeilenanzahl = 10;
var spaltenanzahl = 10;
for(var i = 0; i < zeilenanzahl; i++){
	for(var i=0; i< spaltenanzahl; i++){
		//"("+i+","+j
		console.log(`(${i},${i})`);
	}
}
i = 0;
while(i < 10){
	console.log(i ++);
}

