//Einzeiliger Kommentar
/*
Mehrzeiliger
Kommentar
*/


var variable1 = 'Hallo Welt';
var variable2 = 'zweite Variable';


console.log(variableOhneWert);

variableOhneWert = 12;
variableOhneWert = variableOhneWert * 10;
variableOhneWert *= 10;

var ichBinEinBoolean = true;

var negation = !ichBinEinBoolean;

//Typ anzeigen lassen
console.log("Typ", typeof negation);

var großerBoolean = new Boolean(true);

var func = function(){}

typeof negation //boolean
typeof variableOhneWert //number
typeof func //function
typeof großerBoolean //object

if(typeof negation === 'string')

if(großerBoolean instanceof Boolean)

console.log("Variable ohne Wert", variableOhneWert);
variableOhneWert = "jetzt bin ich ein String";
console.log("Variable umtypisiert" , variableOhneWert);

console.log("Meine Variable hat den Wert: ", variable1);
console.log("Meine Variable2 hat den Wert: ", variable2);

//Hoisting von var-Variable 
var variableOhneWert = 12;

var concatString = "Hallo " + " Welt";
var concatString = "Hallo " + 10;

console.log(10+"1234");

var indexOfA = concatString.indexOf("a");

