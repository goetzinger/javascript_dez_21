$(document).ready(function () {

    //window

    function SeminarVerwaltung() {


        let nodesWithIdA = document.getElementById("a");

        //lokale Varibale in Funktion SeminarVerwaltung
        let nameFld = document.getElementById("name");
        $("#invalidName").hide();
        nameFld.oninput = function (e) {
            console.log(e.target);
        }
        let descriptionFld = document.getElementById("description");

        const saveBtn = document.getElementById("save");
        let trainings = [];

        let addTraining = (event) => {
            $("#invalidName").hide();
            const trainingName = nameFld.value;
            const trainingDescription = descriptionFld.value;
            try {
                const newTraining = new Training(trainingName, trainingDescription);
                showTrainingInEditor();
                trainings.push(newTraining);
                addToTable(newTraining);
            } catch (err) {
                $("#invalidName").show();
            }


        }//end addTraining Function

        function addToTable(training) {
            //if (training instanceof Training) {
            let tr = document.createElement("tr");
            let td1 = document.createElement("td");
            let td2 = document.createElement("td");

            td1.textContent = training.name
            td2.textContent = training.description;
            tr.appendChild(td1);
            tr.appendChild(td2);

            let tableBody = document.getElementById("tablebody");
            tableBody.appendChild(tr);
            $(tr).click(function () {

                showTrainingInEditor(training);
            })
            //}
        }

        function showTrainingInEditor(trainingToShow) {
            $(nameFld).val(trainingToShow ? trainingToShow.name : "");
            $(descriptionFld).val(trainingToShow ? trainingToShow.description : "");
        }


        //Prüfe ob name leer,null,undefinded -->
        //IllegalArgumentException()

        //Training(name, description)
        function Training(name, description) {
            if (!name)
                throw new IllegalArgumentException("name");
            this.name = name;
            this.description = description || "";
        }

        function loadData() {
            let request = new XMLHttpRequest();
            //vor send()
            request.onreadystatechange = function () {
                //0 - Connection nicht initialisiert
                //1 - Connnection offen (initialisiert)
                //2 - Kontaktiert
                //3 - Daten kommen
                //4 - Daten alle empfangen -- request ende
                if (this.readyState === 4) {
                    //wenn webserver --> 200
                    console.log(this.statusText);
                    console.log(this.responseText)
                    let trainingsFromServer = JSON.parse(this.responseText);
                    console.log(trainingsFromServer);
                    trainingsFromServer.forEach(t => {
                        trainings.push(t);
                        addToTable(t);
                    });
                }
            }
            //open connection letzter Parameter 'async'
            request.open("POST", "data/seminare.json", true);
            request.send();
        }

        function loadDataViaJQuery() {
            $.post("data/seminare.json",
                    function (response) {
                        console.log(response)
                        let trainingsFromServer = JSON.parse(response);
                        console.log(trainingsFromServer);
                        trainingsFromServer.forEach(t => {
                            trainings.push(t);
                            addToTable(t);
                        });
                    }
                );
        }

        loadDataViaJQuery();
        //saveBtn.onclick = addTraining;
        saveBtn.addEventListener("click", addTraining);



    }//END Seminarverwaltung

    function IllegalArgumentException(fieldName) {
        this.message = fieldName + " ist Pflichtfeld";
        this.name = "IllegalArgumentException";
    }

    //window == this
    SeminarVerwaltung();
});