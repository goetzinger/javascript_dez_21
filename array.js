var wochentage = ["Montag", "Dienstag","Mittwoch"];

var derzeitigeLaenge = wochentage.length; //3
wochentage[derzeitigeLaenge] = "Donnerstag"; //Eintrag 4

wochentage.push("Freitag"); // L�nge 5

//pop --> am Ende entfernen
//push --> am Ende reinlegen
//shift --> am Anfang entfernen
//unshift --> am Anfang einf�gen
var freitag = wochentage.pop();
var montag = wochentage[0];

console.log(wochentage); //[Montag, Dienstag...]

//Iteration
for(var i = 0; i < wochentage.length; i++){
	var wochentag = wochentage[i];
	console.log("Tag "+i, " : ", wochentag) //Tag 0 : Montag
}

//Iteration Version 2 for-of (ECMA 6)
for(var wochentag of wochentage){
	var index = wochentage.indexOf(wochentag);
	console.log(wochentag);
}

//Iteration Version 3
for(var index in wochentage){
	var wochentag = wochentage[index];
	console.log(wochentag);
}

//Iteration Version 4
wochentage.forEach(function(wochentag){
	console.log(wochentag);
});

wochentage.forEach((wochentag) => console.log(wochentag));

var addition = (summand1,summand2) => summand1+summand2;
addition(12,13);


//Funktionen in Javascript
//(parameterListe) -> rechenvorschrit (parameterListe)

//(a,b) -> return a+b;
//(a,b) -> return a-b;



//Filtern
var arrayMitFilter = [];
for(var i = 0; i< wochentage.length; i++){
	var wochentag = wochentage[i];
	if(wochentag.indexOf("a")>= 0){
		arrayMitFilter.push(wochentag);
	}
}
console.log("Gefiltert nach Tagen mit a ", arrayMitFilter);

var gefilterte = wochentage.filter((wochentag) => wochentag.indexOf("a")>= 0);
console.log("Gefiltert nach Tagen mit a Variante 2 ", gefilterte);

var mappedArray = [];
for(var i = 0; i< wochentage.length; i++){
	var wochentag = wochentage[i];
	switch(wochentag)
	{
		case "Montag": mappedArray.push(1); break;
		case "Dienstag": mappedArray.push(2); break;
	}
}
console.log("Tage nummeriert", mappedArray);

var mappedArray2 = wochentage.map((wochentag) => {
{
		case "Montag": return 1;
		case "Dienstag": return 2;
}});
console.log("Mapped Array", mappedArray2);
