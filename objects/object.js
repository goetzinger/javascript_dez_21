var currentSpeed;

let brandBMW = "BMW"

const bmw330i = {
    model: "330i",
    brand: brandBMW,
    ps: 260,
    maxkmh: 250,
    currentSpeed:0,
    accelerate: function(){
        this.currentSpeed++;
    }
}

console.log(bmw330i.brand); //BMW
 
bmw330i.ps = 300; //eigenschaften beschreiben

bmw330i.accelerate();
console.log(bmw330i.currentSpeed);//1

const missPiggy = {
    name: "Miss Piggy",
    weight: 20,
    fressen: function(){
        this.weight++;
    },
    suhlen: function(){
        this.weight--;
    }
}
missPiggy.fressen();