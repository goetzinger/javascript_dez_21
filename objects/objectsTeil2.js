

const missPiggy = {
    name: "Miss Piggy",
    weight: 20,
    eat: function(){
        this.weight++;
    }
}

function feed(pig){
    pig.eat();
}

console.log("Vorm Essen",missPiggy.weight);
feed(missPiggy);
console.log("Nach dem Essen",missPiggy.weight);

//Rein im JSEditor nicht verboten aber Laufzeitprobleme, da eat() nicht 
//auf String 
//feed("ich bin gar kein Schwein");


function feedAll(pigs){
    pigs.forEach(pig => {
        pig.eat();
    });
}

const peppaWutz = {
    name: "Peppa Wutz",
    weight: 20,
    eat: function(){
        //console.log("This in Pig",this);
        this.weight++;
    }
}

let favoritePigsOfTamia = [missPiggy, peppaWutz];
feedAll(favoritePigsOfTamia);


var einString;
var einander = einString || "Was festes";

//this === window
//Konstruktionfunktion
function Pig(name, weight){
    this.name = name || "Nobody";
    this.weight = weight || 20;

    //closure
    this.eat = function(){
        this.weight++;
    }
    console.log("Was ist this",this);
}

//Neues Leeres Objekt erzeugen und an diesem den Konstruktor ausführen
var p = new Pig("Schweinchen Dick", 100);
p.eat();
console.log("Konstruiertes Schwein nach dem Esssen", p);

p.name = "Neuer Name";
var leeresSchwein = new Pig();

var nurNameSchwein = new Pig("Armes SChweinchen ohne Gewicht");


//Analog zu var p = ...
var pig = new Object();
//pig.Pig("Peppa Wutz",200);



var peppaWUtz2 = new Pig("Peppa Wutz2", 20);

//Operiert auf Window Objekt
Pig("Schweinchen Dick2", 200);

console.log("AUsgabe")