function Pig(name, weight){
    this.name = name || "Nobody";
    this.weight = weight || 20;
    this.eat = function(){
        this.weight++;
    } 
}

function Quader(laenge, breite, hoehe){
    this.laenge = laenge || 5;
    this.breite = breite || 5;
    this.hoehe = hoehe || 5;
}

//Konstruktor mit Konfigurationsobjekt
function ConfigQuader(config){
    this.laenge = config.laenge || 5;
    this.breite = config.breite || 5;
    this.hoehe = config.hoehe || 5;

    this.volumen = function(){
        return this.hoehe * this.breite * this.laenge;
    }
}

let quader = new Quader(5,5,5);

let quader2 = new Quader(5,10,5);

let quader3 = new ConfigQuader({breite:10});
let quader4 = new ConfigQuader({laenge: 11, breite:10});

console.log(quader3.wachseUm1); //undefined

quader3.wachseUm1 = function(){
    this.laenge++;
    this.breite++;
    this.hoehe++;
}

console.log(quader3.wachseUm1); //function....

quader3.wachseUm1();

ConfigQuader.prototype.wachseUm2 = function(){
    this.laenge+=2;
    this.breite+=2;
    this.hoehe+=2;
}

quader4.wachseUm2();
quader4.prototype

//quader4 --> prototyp.wachseUm2 <-- quader3