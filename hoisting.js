function outputGlobalVar(){
	let globaleVariable = "Anderer Wert";
	globaleVariable = "NeuerWert";
	console.log("Innen", globaleVariable);
}



var globaleVariable;
outputGlobalVar(); // undefined

globaleVariable = "Ein Wert";
console.log("Aussen 1", globaleVariable);
outputGlobalVar(); //"Ein Wert"
console.log("Aussen 2", globaleVariable);