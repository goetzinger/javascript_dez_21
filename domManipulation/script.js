$(document).ready(function(){
    let counter = 1;
    function addListItem(){
        counter++;
        addToULMitJQuery(counter);
        addToTable(counter);
        function addToUL(currentCounter) {
            //1. Element erstellen
            //<li></li>
            let li = document.createElement("li");
            //<li id="42">2. Element</li>
            //2. Inhalt und Attribute wenn nötig
            li.textContent = currentCounter + ". Element";
            //li.setAttribute("style", "background-color: blue; color: white;");
            $(li)
            //3. in den DOM einfügen
            //Hole den Parent
            let ulElement = document.getElementById("list");
            //Füge als weiteres Kind ein
            ulElement.appendChild(li);
            li.setAttribute("style", "background-color: " + ((currentCounter % 2) === 0 ? "grey": "white"))
        }

        function addToULMitJQuery(currentCounter) {
            //<li id="10">10. Element</li> 
           var li = $("#list").append(`<li id="${currentCounter}">${currentCounter}. Element </li>`);
           $(`#${currentCounter}`).click(function(){
               $("#list .selected").removeClass("selected");
               $(this).addClass("selected");
           })
        }


        function addToTable(currentCounter){
            let tr = document.createElement("tr");
            let td1 = document.createElement("td");
            let td2 = document.createElement("td");

            td1.textContent = currentCounter +".";
            td2.textContent = "Element";
            tr.appendChild(td1);
            tr.appendChild(td2);
            
            let tableBody = document.getElementById("tablebody");
            tableBody.appendChild(tr);

        }

        
    }

    function setBackgroundColor(){
        $('#list li').addClass("grey");
    }

    //let addButton = document.getElementById("add");
    let addButton = $("#add");//suche byId
    // addButton.addEventListener("click", addListItem);
    addButton.click(addListItem);
    // $("#add").click(addListItem);

    $("#colorButton").click(setBackgroundColor)

    $("#findSelected").click(function(){
        console.log($("#list li.selected.grey"));
    })
});