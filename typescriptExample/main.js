"use strict";
var ausgabe = 25;
var ausgabeTypisiert = 25;
//ausgabeTypisiert = "25";
console.log(ausgabe);
function oneFunctionUsingString(param) {
}
oneFunctionUsingString(ausgabeTypisiert);
oneFunctionUsingString("ein string");
//oneFunctionUsingString(new Date());
class Pig {
    constructor(name) {
        this.name = name;
    }
    ausgabe() {
        return this.name;
    }
}
let pig = new Pig("Hans Martin");
pig.ausgabe();
