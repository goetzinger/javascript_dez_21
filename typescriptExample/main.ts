var ausgabe = 25;

var ausgabeTypisiert : number = 25;
//ausgabeTypisiert = "25";

console.log(ausgabe);

function oneFunctionUsingString(param: string | number){

}

oneFunctionUsingString(ausgabeTypisiert);
oneFunctionUsingString("ein string");
//oneFunctionUsingString(new Date());


class Pig{

    constructor(private readonly name:string){
    }

    ausgabe(){
        return this.name;
    }
}

let pig : Pig= new Pig("Hans Martin");
pig.ausgabe();


