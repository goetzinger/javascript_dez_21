function ConfigQuader(config){
    this.laenge = config.laenge || 5;
    this.breite = config.breite || 5;
    this.hoehe = config.hoehe || 5;

    this.volumen = function(){
        return this.hoehe * this.breite * this.laenge;
    }
}